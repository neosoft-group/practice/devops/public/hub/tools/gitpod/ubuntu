Feature: Acceptance Criteria for 'taskfile' Test Files

  Scenario: Failure Due to Missing Main Taskfile
    Given the main Taskfile is not present
    When the integrity verifier script is run
    Then the script should fail, indicating the main Taskfile is required for successful validation

  Scenario: Failure Due to Absence of taskfiles Directory
    Given the taskfiles directory does not exist
    When the integrity verifier script is run
    Then the script should fail, indicating the missing directory as the cause
