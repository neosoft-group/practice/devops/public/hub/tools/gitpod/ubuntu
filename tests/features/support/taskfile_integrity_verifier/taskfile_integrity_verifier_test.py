# flake8: noqa: E501
import os
import subprocess
import shutil
from pytest_bdd import scenarios, given, then, when

# Constants
BASE_PATH = "tests/features/support/taskfile_integrity_verifier/tmp"
MAIN_TASKFILE_PATH = f"{BASE_PATH}/Taskfile.yml"
DOMAIN_TASKFILE_DIR = f"{BASE_PATH}/taskfiles"
DOMAIN_TASKFILE_PATH = f"{DOMAIN_TASKFILE_DIR}/Sample.Taskfile.yml"
SCRIPT_FULL_PATH = ".config/taskfile/helpers/taskfile_integrity_verifier.py"

# Define Scenarios
scenarios('baseline.feature', 'acceptance.feature')

def init_directories():
    """
    Initialize necessary directories.
    """
    os.makedirs(DOMAIN_TASKFILE_DIR, exist_ok=True)

def write_taskfile(path, content):
    """
    Write content to a taskfile at the specified path.
    """
    with open(path, "w") as file:
        file.write(content)

def setup_main_taskfile():
    """
    Setup the main Taskfile.
    """
    content = """
---
version: "3"

includes:
    sample: taskfiles/Sample.Taskfile.yml

tasks:
    run:
        desc: Run test suite
        cmds:
        - task: sample:run
    """
    write_taskfile(MAIN_TASKFILE_PATH, content)
    return MAIN_TASKFILE_PATH

def setup_domain_taskfile():
    """
    Setup the domain Taskfile.
    """
    content = """
---
version: "3"

tasks:
    run:
        desc: Execute a demonstration task
        cmds:
            - task: echo-demo

    echo-demo:
        desc: Display a generic message
        cmds:
            - echo "This is a demonstration message"
    """
    write_taskfile(DOMAIN_TASKFILE_PATH, content)
    return DOMAIN_TASKFILE_PATH

@given('a valid main Taskfile is present')
def given_main_taskfile():
    init_directories()
    return setup_main_taskfile()

@given('a valid domain Taskfile is also available')
def given_domain_taskfile():
    return setup_domain_taskfile()

@when('the integrity verifier script is run', target_fixture="script_result")
def execute_script():
    result = subprocess.run(['python3', SCRIPT_FULL_PATH, MAIN_TASKFILE_PATH, DOMAIN_TASKFILE_DIR], capture_output=True, text=True)
    return result

@then('all checks should pass indicating successful validation')
def then_checks_pass(script_result):
    assert "[taskfile_integrity_verifier]✅ All checks passed." in script_result.stdout
    expected_message = f"[taskfile_integrity_verifier]✅ All tasks are present in 'run' of {DOMAIN_TASKFILE_PATH}."
    assert expected_message in script_result.stdout

@given('the main Taskfile is not present')
def given_no_main_taskfile():
    init_directories()
    if os.path.exists(MAIN_TASKFILE_PATH):
        os.remove(MAIN_TASKFILE_PATH)
    return MAIN_TASKFILE_PATH

@then('the script should fail, indicating the main Taskfile is required for successful validation')
def then_main_taskfile_failure(script_result):
    assert script_result.returncode != 0
    expected_error_message = f"❌ Error: The main taskfile '{MAIN_TASKFILE_PATH}' does not exist."
    assert expected_error_message in script_result.stderr

@given('the taskfiles directory does not exist')
def given_no_taskfiles_directory():
    init_directories()
    setup_main_taskfile()
    if os.path.exists(DOMAIN_TASKFILE_DIR):
        shutil.rmtree(DOMAIN_TASKFILE_DIR)
    return DOMAIN_TASKFILE_DIR

@then('the script should fail, indicating the missing directory as the cause')
def then_directory_failure(script_result):
    assert script_result.returncode != 0
    expected_error_message = f"❌ Error: The taskfiles directory '{DOMAIN_TASKFILE_DIR}' does not exist or is not a directory."
    assert expected_error_message in script_result.stderr
