## 0.2.0 (2024-01-05)

### Feat

- add .cz.yaml configuration file for commitizen to enforce conventional commit messages and manage versioning using semver

## 0.1.0 (2024-01-05)

### Feat

- **.gitpod.yml**: add tasks configuration to automate dev server setup on Gitpod for improved developer experience
- **tests**: add Performance.Taskfile.yml for performance testing
- **tests**: add Installation.Taskfile.yml for Docker image verification
- **tests**: add Image.Taskfile.yml to verify Docker image size
- **tests**: add Container.Taskfile.yml for Docker image validation
- **tests**: add taskfile_integrity_verifier.py to verify taskfile integrity
- **tests**: add taskfile integrity verifier tests
- **tests**: add baseline.feature for taskfile integrity verification This new feature file outlines the scenario for successful execution of the integrity verifier script. This is to ensure that both main and domain Taskfiles are valid and pass all checks.
- **tests**: add acceptance criteria for taskfile integrity verifier
- **tests**: add Taskfile.yml for task management and testing
- **Dockerfile**: add Dockerfile to containerize the application
- **bootstrap.sh**: add new bootstrap script to automate system setup
- **yamllint**: add Taskfile.yml for yaml linting
- **Taskfile.yml**: add bash completion installation task to automate the process of setting up bash completion for task commands
- **Taskfile.yml**: add new tasks for system configuration
- **Taskfile.yml**: add new taskfile for Python environment setup
- **.config/python**: add .python-version file to specify Python version as 3.11 for consistent environment setup
- **Taskfile.yml**: add new taskfile for NodeJS installation and dependencies setup
- **Taskfile.yml**: add new taskfile for Go version management and installation
- **Taskfile.yml**: add build, tag, and push tasks for Docker image management
- **flake8/Taskfile.yml**: add new task for linting Python files with flake8 in Docker
- **flake8**: add flake8 configuration file to exclude certain directories from linting
- **docker**: add taskfile for Docker installation on Ubuntu Linux
- **docker**: add new taskfile for Docker system prune
- **lint.Taskfile.yml**: add new linting taskfile for Dockerfiles
- **docker**: add taskfile to list all Dockerfiles in the project
- **docker**: add Taskfile.yml to manage docker tasks
- **docker**: add copier-answers.yml to track source path and commit version This file will help in tracking the source path and commit version of the docker configuration.
- **Taskfile.yml**: add new taskfile for Copier and dependencies installation
- **requirements.txt**: add copier package to manage project templates more efficiently
- **commitizen**: add Taskfile.yml for automated Commitizen setup
- **commitizen**: add requirements.txt file with commitizen version to ensure consistent commit messages across contributors
- add Taskfile.yml for orchestrating DevSecOps workflows
- **.gitpod.yml**: add Gitpod configuration file to set up cloud-based development environment using a specific Docker image for consistency and tailoring
- add .gitlab-ci.yml for CI/CD pipeline configuration
- **.gitignore**: add .gitignore file to exclude unnecessary files from version control
- **.env.dist**: add DOCKER_REGISTRY_USER and DOCKER_REGISTRY_PASSWORD to .env.dist file to support Docker registry authentication
- **docker**: add .dockerignore file to exclude unnecessary files from Docker build context, improving build speed and reducing image size
