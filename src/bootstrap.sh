#!/bin/bash

# Use non-interactive frontend for debconf
export DEBIAN_FRONTEND=noninteractive

check_and_install_sudo() {
    if ! command -v sudo &>/dev/null; then
        apt-get update -y
        apt-get install -y sudo
    fi
}

upgrade_system() {
    sudo apt-get update
    sudo apt-get upgrade -y
}

install_packages() {
    PACKAGES=(
        apt-utils
        ca-certificates
        curl
        git
        git-lfs
        gnupg
        lsb-release
        make
        sudo
        uuid
        wget
    )

    sudo apt-get install -y --no-install-recommends "${PACKAGES[@]}"
}

install_task() {
    curl -sL https://taskfile.dev/install.sh | sudo sh -s -- -d -b /usr/local/bin

    task --version
}

check_and_install_sudo
upgrade_system
install_packages
install_task
task bootstrap
