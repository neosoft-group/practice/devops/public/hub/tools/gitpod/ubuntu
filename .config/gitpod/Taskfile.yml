---
version: "3"

vars:
  # yamllint disable rule:line-length
  DOCKER_TARGET_REGISTRY_TAG:
    sh: |
      # Verification against GitLab CI variables
      if [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
        RELEASE_TAG="latest"
      elif [ ! -z "$CI_COMMIT_TAG" ]; then
        RELEASE_TAG="$CI_COMMIT_TAG"
      elif [ "$CI_PIPELINE_SOURCE" == "merge_request_event" ]; then
        RELEASE_TAG="$CI_COMMIT_REF_SLUG"
      fi

      # Utilising the RELEASE_TAG variable if set; otherwise, generating a unique tag
      if [ ! -z "$RELEASE_TAG" ]; then
        echo "$RELEASE_TAG"
      else
        # Ascertain the availability of uuid
        if command -v uuid >/dev/null 2>&1; then
          # In the event uuid is installed
          uuid
        else
          # In its absence, resort to a fallback method, such as date and time
          echo "fallback-$(date +%Y%m%d%H%M%S)"
        fi
      fi
  # yamllint enable rule:line-length
  DOCKER_REGISTRY_USER:
    sh: |
      [ -f .env ] && export $(grep -v '^#' .env | xargs)
      if [ ! -z "$DOCKER_REGISTRY_USER" ]; then
        echo "$DOCKER_REGISTRY_USER"
      else
        echo "$CI_REGISTRY_USER"
      fi
  # yamllint disable rule:line-length
  DOCKER_TARGET_REGISTRY_IMAGE:
    sh: |
      if [ -z "$CI_REGISTRY_IMAGE" ]; then
        echo "registry.gitlab.com/neosoft-group/practice/devops/public/hub/gitpod/ubuntu"
      else
        echo "$CI_REGISTRY_IMAGE"
      fi
  # yamllint enable rule:line-length
  DOCKER_REGISTRY_PASSWORD:
    sh: |
      [ -f .env ] && export $(grep -v '^#' .env | xargs)
      if [ ! -z "$DOCKER_REGISTRY_PASSWORD" ]; then
        echo "$DOCKER_REGISTRY_PASSWORD"
      else
        echo "$CI_REGISTRY_PASSWORD"
      fi

tasks:
  build:
    summary: |
      🏷️  Task Description:
      The 'build' task is quintessentially designed to construct Docker images with an exquisite degree of customisation and precision.
      Utilising this task,
      one may seamlessly specify the image name,
      tag,
      path to Dockerfile,
      and a plethora of additional options to tailor the Docker build process.

      🎚️ Variables Explained:
      - DOCKER_IMAGE_NAME: Essential for denoting the identity of the resulting image.
      - DOCKER_IMAGE_TAG: Used to distinguish between different versions or configurations.
      - DOCKER_DOCKERFILE_PATH: Vital for guiding the build process to the correct set of instructions.
      - DOCKER_BUILDKIT_ENABLED: Determines the employment of Docker's improved BuildKit.
      - DOCKER_BUILD_OPTIONS: A flexible receptacle for custom build command options.

      🔐 Preconditions:
      Ensures all necessary variables are set before the build process commences,
      a paramount step to avoid failures and ensure informed execution.

      🔄 Execution:
      Executes a Docker build command,
      ingeniously crafting your Docker image with the specified parameters and options,
      tailored to your exact specifications.

      📝 Note:
      Crafted with flexibility and robustness in mind,
      empowering users to define parameters and options to suit their diverse needs.
      A testament to the bespoke,
      meticulous nature of modern software development practices.
    preconditions:
      - sh: "[[ ! -z {{.DOCKER_IMAGE_NAME}} ]]"
        msg: "❌ One must ensure that the DOCKER_IMAGE_NAME variable is duly set prior to proceeding."
      - sh: "[[ ! -z {{.DOCKER_IMAGE_TAG}} ]]"
        msg: "❌ It is imperative that the DOCKER_IMAGE_TAG variable is specified beforehand."
      - sh: "[[ ! -z {{.DOCKER_DOCKERFILE_PATH}} ]]"
        msg: "❌ Kindly ascertain that the DOCKER_DOCKERFILE_PATH variable is correctly defined."
      - sh: "[[ ! -z {{.DOCKER_BUILDKIT_ENABLED}} ]]"
        msg: "❌ One must verify that the DOCKER_BUILDKIT_ENABLED variable is appropriately established."
    cmds:
      - docker build {{.DOCKER_BUILD_OPTIONS}} --tag {{.DOCKER_IMAGE_NAME}}:{{.DOCKER_IMAGE_TAG}} --file {{.DOCKER_DOCKERFILE_PATH}} .

  tag:
    deps: [build]
    summary: |
      🏷️  Task Description:
      The 'tag' task is specifically orchestrated to label Docker images,
      facilitating a refined and systematic categorisation.
      By utilising this task,
      practitioners may adeptly assign tags to their images,
      delineating between various stages,
      versions,
      or environments.

      🎚️ Variables Explained:
      - DOCKER_IMAGE_NAME: Indispensable for identifying the image to be tagged.
      - DOCKER_IMAGE_TAG: Specifies the tag to be applied, reflecting version or configuration nuances.
      - DOCKER_TARGET_REGISTRY_IMAGE: Defines the registry path where the image resides, crucial for accurate tagging.
      - DOCKER_TARGET_REGISTRY_TAG: Specifies the tag for the image in the target registry, allowing for distinct versioning or naming in different registries.
      - DOCKER_TAG_OPTIONS: A flexible receptacle for additional tag command options.

      🔐 Preconditions:
      Asserts the prerequisite setting of essential variables,
      safeguarding against inadvertent omissions and ensuring a successful tagging routine.

      🔄 Execution:
      Performs the tagging operation,
      adroitly appending the specified tag to the designated image,
      thus readying it for its next phase in the lifecycle or deployment.

      📝 Note:
      This task is designed with precision and adaptability in mind,
      allowing for a streamlined and error-free tagging process.
      It underscores the importance of meticulous management in image repositories and deployment workflows.
    preconditions:
      - sh: "[[ ! -z {{.DOCKER_IMAGE_NAME}} ]]"
        msg: "❌ The DOCKER_IMAGE_NAME variable must be properly defined prior to any tagging endeavour."
      - sh: "[[ ! -z {{.DOCKER_IMAGE_TAG}} ]]"
        msg: "❌ It is crucial that the DOCKER_IMAGE_TAG variable is established beforehand."
      - sh: "[[ ! -z {{.DOCKER_TARGET_REGISTRY_IMAGE}} ]]"
        msg: "❌ Ascertain that the DOCKER_TARGET_REGISTRY_IMAGE variable is correctly set for precise image referencing."
      - sh: "[[ ! -z {{.DOCKER_TARGET_REGISTRY_TAG}} ]]"
        msg: "❌ Ensure the DOCKER_TARGET_REGISTRY_TAG variable is established for accurate tagging in the target registry."
    cmds:
      - docker tag {{.DOCKER_TAG_OPTIONS}} {{.DOCKER_IMAGE_NAME}}:{{.DOCKER_IMAGE_TAG}} {{.DOCKER_TARGET_REGISTRY_IMAGE}}:{{.DOCKER_TARGET_REGISTRY_TAG}}

  push:
    summary: |
      🏷️  Task Description:
      The 'push' task is expertly tailored to upload Docker images to specified registries,
      marking the culmination of the image's preparation.
      This task is pivotal for making the image available for deployment or sharing across teams and systems.

      🎚️ Variables Explained:
      - DOCKER_TARGET_REGISTRY_IMAGE: The registry path where the image will be pushed, critical for correct placement.
      - DOCKER_TARGET_REGISTRY_TAG: The specific tag of the image being pushed, ensuring version control and proper identification in the registry.
      - DOCKER_PUSH_OPTIONS: A flexible receptacle for additional push command options.
      - DOCKER_REGISTRY_USER: Username for logging into the Docker registry.
      - DOCKER_REGISTRY_PASSWORD: Password for logging into the Docker registry.

      🔐 Preconditions:
      Verifies the availability of necessary details for a successful image push,
      ensuring that the image's destination, credentials, and relevant details are in order prior to the push operation.

      🔄 Execution:
      Engages in pushing the image to the designated registry with the specified tag,
      effectively distributing the image for subsequent deployment or utilization.

      📝 Note:
      Meticulously crafted to provide a seamless and error-minimizing pushing process,
      this task underscores the importance of distribution in the lifecycle of containerized applications.
    preconditions:
      - sh: "[[ ! -z {{.DOCKER_TARGET_REGISTRY_IMAGE}} ]]"
        msg: "❌ The DOCKER_TARGET_REGISTRY_IMAGE variable must be set to specify the target registry."
      - sh: "[[ ! -z {{.DOCKER_REGISTRY_USER}} ]]"
        msg: "❌ DOCKER_REGISTRY_USER must be set for login credentials."
      - sh: "[[ ! -z {{.DOCKER_REGISTRY_PASSWORD}} ]]"
        msg: "❌ DOCKER_REGISTRY_PASSWORD must be set for login credentials."
      - sh: "[[ ! -z {{.DOCKER_TARGET_REGISTRY_TAG}} ]]"
        msg: "❌ The DOCKER_TARGET_REGISTRY_TAG variable is required to specify the tag of the image in the target registry."
    cmds:
      - cmd: docker login -u {{.DOCKER_REGISTRY_USER}} -p {{.DOCKER_REGISTRY_PASSWORD}} {{.DOCKER_TARGET_REGISTRY_IMAGE}}
      - docker push {{.DOCKER_PUSH_OPTIONS}} {{.DOCKER_TARGET_REGISTRY_IMAGE}}:{{.DOCKER_TARGET_REGISTRY_TAG}}
