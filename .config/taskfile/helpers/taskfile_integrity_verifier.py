# flake8: noqa: E501
import os
import sys
import yaml


def check_tasks_in_taskfile(file_path, task_name='run'):
    # 📚 Loading the Taskfile
    # This process is essential for verifying task configurations.
    with open(file_path, 'r') as file:
        data = yaml.safe_load(file)

        # 🚨 Ensuring the specified task exists
        # This is crucial to maintain the integrity of the task workflow.
        if task_name not in data['tasks']:
            print(f"The task '{task_name}' is not defined in {file_path}.")
            return False

        # 🧐 Identifying and comparing tasks
        # This step is imperative for ensuring all subtasks are properly referenced.
        defined_tasks = set(data['tasks'].keys()) - {task_name}
        tasks_in_task = set(task['task'] for task in data['tasks'][task_name]['cmds'] if 'task' in task)
        missing_tasks = defined_tasks - tasks_in_task

        # 📢 Reporting missing tasks
        # This feedback is vital for debugging and rectifying task dependencies.
        if missing_tasks:
            print(f"Missing tasks in '{task_name}' of {file_path}: {missing_tasks}")
            return False
        else:
            print(f"[taskfile_integrity_verifier]✅ All tasks are present in '{task_name}' of {file_path}.")
            return True

def check_main_taskfile_calls_run(main_taskfile, taskfile_directory):
    # 🔍 Examining the main Taskfile
    # This scrutiny ensures that all taskfile 'run' tasks are appropriately invoked.
    with open(main_taskfile, 'r') as file:
        data = yaml.safe_load(file)
        main_tasks = set(cmd.get('task') for cmd in data['tasks']['run']['cmds'] if 'task' in cmd)

    success = True
    # 🔄 Iterating through all taskfiles
    # This thorough check guarantees comprehensive coverage of task integrations.
    for taskfile in os.listdir(taskfile_directory):
        if taskfile.endswith('.Taskfile.yml'):
            prefix = taskfile.replace('.Taskfile.yml', '').lower()

            # 🚫 Identifying missing task calls
            # This detection is fundamental to ensuring a cohesive task structure.
            if f"{prefix}:run" not in main_tasks:
                print(f"Main Taskfile does not call 'run' task of {taskfile}")
                success = False
    return success

if __name__ == "__main__":
    # ✅ Validating script usage
    # This confirmation ensures the script is executed with the necessary arguments.
    if len(sys.argv) <= 2:
        # Providing more detailed guidance for the user
        if len(sys.argv) == 1:
            print("Error: No arguments provided.")
        elif len(sys.argv) == 2:
            print("Error: Missing taskfile directory argument.")

        print("Usage: python3 taskfile_integrity_verifier.py <main_taskfile_path> <taskfile_directory>")
        print("python3 .config/taskfile/helpers/taskfile_integrity_verifier.py tests/Taskfile.yml tests")
        sys.exit(1)

    main_taskfile = sys.argv[1]
    taskfile_directory = sys.argv[2]

    # 🛣️ Verifying file path existence
    # This step is critical for the script's operation with valid paths.
    if not os.path.exists(main_taskfile):
        print(f"❌ Error: The main taskfile '{main_taskfile}' does not exist.", file=sys.stderr)
        sys.exit(1)
    if not os.path.isdir(taskfile_directory):
        print(f"❌ Error: The taskfiles directory '{taskfile_directory}' does not exist or is not a directory.", file=sys.stderr)
        sys.exit(1)

    # 🔄 Checking tasks in each Taskfile
    # This loop is central to validating individual taskfile configurations.
    for taskfile in os.listdir(taskfile_directory):
        if taskfile.endswith('.Taskfile.yml'):
            file_path = os.path.join(taskfile_directory, taskfile)
            if not check_tasks_in_taskfile(file_path):
                sys.exit(1)

    # 🔗 Ensuring main Taskfile calls 'run'
    # This validation is key to maintaining the hierarchical task structure.
    if not check_main_taskfile_calls_run(main_taskfile, taskfile_directory):
        sys.exit(1)

    # 🎉 Confirming successful checks
    # This message signifies the script's successful execution and validation.
    print("[taskfile_integrity_verifier]✅ All checks passed.")
